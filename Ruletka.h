#pragma once
#include "Gra.h"
class Ruletka : public Gra
{
public:

	Ruletka(Dialog * dialogPtr);
	void Graj();

	void grajWKolory(string kolor, int stawka);

	void grajWParzystosc(int korekta, int stawka);

	


private:
	string SprawdzKolor(int wylosowanaLiczba);
	int CoObstawiaszRuletka();
	void GraWLiczby(int stawka);
	int WylosujLiczbe();
};

