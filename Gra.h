#pragma once
#include "Zawodnik.h"
#include "Dialog.h"
#include "Narzedzia.h"



class Gra
{

public:
	//konstruktor
	Gra(Dialog* dialogPtr);

	//to sa metody abstrakcyjne
	virtual void Graj() = 0;
	virtual bool Podejdz(Zawodnik* zawodnik);
	virtual bool Odejdz();



private:

protected:
	Dialog* dialog;
	Zawodnik* gracz;
	bool czyWyplacalny();
	

};

