#pragma once
#include <string>
#include <iostream>

using namespace std;


class ObslugaPliku
{

public:
	ObslugaPliku(string nazwaPliku);
	string wczytajPlik();
	void zapiszDoPliku(string zawPliku);

private:
	string pobierzSciezke();
	string sciezka;

};

