#pragma once
#include "Zawodnik.h"

class Dialog
{
public:
	int wyborGryZMenu();
	Zawodnik* PrezentacjaDanychZawodnika(Zawodnik* zawodnik);
	void PrezentajaPortfelaZawodnika(Zawodnik* zawodnik);
	int  PobierzStawke(int masymalnaStawka);
	void PoinformujoWygranej(int kwota);
	void PoinformujoPrzegranej(int kwota);
	bool CzyGraszDalej();
	int PobieranieCyfryOdUzytkownika(int min, int max);
	int PobieranieLiczbyOdUzytkownika(int min, int max);
	void Oczekiwanie();


////Bedzie widoczna dla klas dziedziczacyh po tej kasie, 
////ale nie mozna jej uzyc po stworzniu obiektu tej klasy
protected:




};

