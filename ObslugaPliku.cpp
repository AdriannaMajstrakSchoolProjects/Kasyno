#include "stdafx.h"
#include "ObslugaPliku.h"
#include <direct.h>
#include <fstream>


ObslugaPliku::ObslugaPliku(string nazwaPliku)
{
	sciezka=pobierzSciezke() + "\\" + nazwaPliku;
}

string ObslugaPliku::wczytajPlik()
{
	string zawPliku="";
	string linia;
	ifstream* strumien = new ifstream();
	strumien->open(sciezka);
	bool pierwszaLinia = true;
	
	while (strumien->good())
	{
		linia = "";
		getline(*strumien, linia);

		if (linia != "")
		{
			if (pierwszaLinia)
			{
				pierwszaLinia = false;
			}
			else
			{
				zawPliku += "\r\n";
			}
			
			zawPliku += linia;
		}
		else
		{
			zawPliku += "\r\n";
		}
	}

	strumien->close();
	delete strumien;
	return zawPliku;	
}

void ObslugaPliku::zapiszDoPliku(string zawPliku)
{
	ofstream* strumien = new ofstream();
	strumien->open(sciezka);

	*strumien << zawPliku;

	strumien->close();
	delete strumien;
}

string ObslugaPliku::pobierzSciezke()
{
	return _getcwd(NULL, 0);
}


