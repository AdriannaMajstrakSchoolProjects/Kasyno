#include "stdafx.h"
#include "Gra.h"
#include "Zawodnik.h"
#include "Dialog.h"
#include "Narzedzia.h"


Gra::Gra(Dialog* dialogPtr)
{
	dialog = dialogPtr;
	

}

bool Gra::Podejdz(Zawodnik* zawodnik)
{
	if (gracz == nullptr)
	{
		gracz = zawodnik;
		return true;
	}
	else
	{
		return false;
	}
}

bool Gra::Odejdz()
{
	if (gracz == nullptr)
	{
		return false;
	}
	else
	{
		gracz = nullptr;
		return true;
	}
}

bool Gra::czyWyplacalny()
{
	if (gracz->hajs <= 0)
	{
		cout << "Skonczyly sie srodki. Zapraszamy ponownie" << endl;
		return false;
	}

	return true;
}
