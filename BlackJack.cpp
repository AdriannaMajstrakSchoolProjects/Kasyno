#include "stdafx.h"
#include "BlackJack.h"
#include <iostream>
#include <string>
#include <ctime>
using namespace std;

BlackJack::BlackJack(Dialog * dialogPtr) : Gra(dialogPtr)
{
}

void BlackJack::Graj()
{
	int suma = 0;
	cout << "Grsz w Black Jack'a." << endl;
	cout << "Dobieraj karty tak by uzbierac liczbe punktow najblizsza 21, " << endl;
	cout << "ale nie wi�ksz�" << endl;
	cout << "Wygrana-podwojenie stawki" << endl;

	dialog->PrezentajaPortfelaZawodnika(gracz);
	int stawka = dialog->PobierzStawke(gracz->hajs);

	cout << "Pierwsza karta" << endl;

	do
	{
		suma += LosujKarte(suma);
		cout << "Masz " << suma << " punktow" << endl;

		if (suma == 21)
		{
			cout << "Oczko! WYGRA�ES!";
			gracz->dodajHajs(stawka);
			break;
		}
		else if (suma > 21)
		{
			cout << "Przykro mi, przegrales ;(" << endl;
			gracz->odejmijHajs(stawka);
			break;
		}
		
	} while (CzyDacKolejnaKarte());

	if (suma<21)
	{
		suma+=LosujKarte(suma);

		if (suma <= 21)
		{
			cout << "Przegra�es, jeszcze jedna karta a bylbys krok blizej 21" << endl;
			gracz->odejmijHajs(stawka);
		}
		else
		{
			cout << "Wygrales! Jeszcze jedna karta i przekroczylbyssume 21";
			gracz->dodajHajs(stawka);
		}
	}

	if (czyWyplacalny() && dialog->CzyGraszDalej())
	{
		Graj();
	}
	





}

int BlackJack::LosujKarte(int sumaPunktow)
{
	
	int wylosowanaLiczba = Narzedzia::LosujLiczbe(2, 14);
	if (wylosowanaLiczba <= 10)
	{
		cout << wylosowanaLiczba<< endl;
		return wylosowanaLiczba;
	}
	else if(wylosowanaLiczba==11)
	{
		cout << "Walet" << endl;
		return 10;
	}
	else if (wylosowanaLiczba == 12)
	{
		cout << "Dama" << endl;
		return 10;
	}
	else if (wylosowanaLiczba == 13)
	{
		cout << "Krol" << endl;
		return 10;
	}
	else if (wylosowanaLiczba == 14)
	{
		cout << "As" << endl;

		if (sumaPunktow>10) 
		{
			return 1;
		}
		else
		{
			return 11;
		}
	}
	return 0;
}

bool BlackJack::CzyDacKolejnaKarte()
{
	
	cout << "Czy chcesz kolejna karte?" << endl;
	cout << "1 - tak" << endl;
	cout << "2 - nie" << endl;	

	return dialog->PobieranieCyfryOdUzytkownika(1, 2) == 1;
		
}
