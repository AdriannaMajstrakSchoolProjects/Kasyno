#pragma once
#include <string>
#include "ObslugaPliku.h"
#include <vector>
#include "Zawodnik.h"

using namespace std;

class Top100
{

public:
	Top100(ObslugaPliku* objObslugaPliku);

	void Sortowanie(vector<Zawodnik*> &zawodnicy);
	void ZapiszWynik(string imie, int wynik);

private:
	ObslugaPliku* obslugaPliku;
	
};

