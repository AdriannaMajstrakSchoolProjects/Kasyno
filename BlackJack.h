#pragma once
#include "Gra.h"
#include "Dialog.h"

class BlackJack : public Gra
{
public:

	BlackJack(Dialog * dialogPtr);

	void Graj();
	int LosujKarte(int sumaPunktow);

	bool CzyDacKolejnaKarte();
	
};

