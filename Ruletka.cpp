#include "stdafx.h"
#include "Ruletka.h"
#include "Narzedzia.h"
#include <iostream>
#include <string>
#include <ctime>
#include <windows.h> 

using namespace std;

Ruletka::Ruletka(Dialog * dialogPtr) :Gra(dialogPtr)
{

}

void Ruletka::Graj()
{

	cout << "Grsz w Ruletke." <<endl;
	cout << "Obstawianie koloru -> podwojenie stawki" << endl;
	cout << "Obstawianie parzysty/nieparzysty -> podwojenie stawki" << endl;
	cout << "Obstawianie liczby -> stawka x 10" << endl;

	dialog->PrezentajaPortfelaZawodnika(gracz);
	int stawka = dialog->PobierzStawke(gracz->hajs);



	switch (this->CoObstawiaszRuletka())
	{
	case 1: 
		grajWKolory("czerwone", stawka);
		break;
	case 2:
		grajWKolory("czarne", stawka);
		break;
	case 3:
		grajWParzystosc(0, stawka);
		break;
	case 4:
		grajWParzystosc(1, stawka);
		break;
	case 5:
		GraWLiczby(stawka);
	default:
		break;
	}

	if (czyWyplacalny() && dialog->CzyGraszDalej())
	{
		Graj();
	}
}

void Ruletka::grajWKolory(string kolor, int stawka)
{
	cout << "Wybrales kolor " << kolor <<endl;
	cout << "Rzucam kulka " << endl;

	dialog->Oczekiwanie();



	if (kolor == SprawdzKolor(WylosujLiczbe()))
	{
		cout << "weeee" << endl;
		gracz->dodajHajs(stawka);
		dialog->PoinformujoWygranej(2*stawka);
	}
	else
	{
		cout << "uuuuuu" << endl;
		gracz->odejmijHajs(stawka);
		dialog->PoinformujoPrzegranej(stawka);
	}
}

void Ruletka::grajWParzystosc(int korekta, int stawka)
{
	if (!korekta)
	{
		cout << "Wybrales liczby parzyste" << endl;
	}
	else
	{
		cout << "Wybrales liczby nieparzyste" << endl;
	}
	cout << "Rzucam kulka " << endl;

	dialog->Oczekiwanie();

	int liczba=WylosujLiczbe();
	SprawdzKolor(liczba);

	if ((liczba+korekta) % 2 == 0)
	{
		gracz->dodajHajs(stawka);
		dialog->PoinformujoWygranej(2*stawka);
	}
	else
	{
		gracz->odejmijHajs(stawka);
		dialog->PoinformujoPrzegranej(stawka);
	}


}

string Ruletka::SprawdzKolor(int wylosowanaLiczba)
{
	int czerwone[] = { 0,1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36 };
	
	for (int i = 0; i <18; i++)
	{
		if (wylosowanaLiczba == czerwone[i])
		{
			
			cout <<" czerwone ";
			return "czerwone";	
		}
	}
	
	cout << " czarne ";
	return "czarne";
}

int Ruletka::CoObstawiaszRuletka()
{
	cout << "Co obstawiasz?" << endl;
	cout << "1 - kolor czerwony" << endl;
	cout << "2 - kolor czarny" << endl;
	cout << "3 - parzysta" << endl;
	cout << "4 - nieparzysta" << endl;
	cout << "5 - b�d� obstawial konkretna liczbe" << endl;

	return dialog->PobieranieCyfryOdUzytkownika(1, 5);
}

void Ruletka::GraWLiczby(int stawka)
{
	int liczba;
	int wylosowanaLiczba;
	cout << "Podaj liczbe od 0 do 36 ktora obstawiasz" << endl;
    liczba = dialog->PobieranieLiczbyOdUzytkownika(0, 36);
	wylosowanaLiczba = WylosujLiczbe();
	SprawdzKolor(wylosowanaLiczba);

	if (liczba  == wylosowanaLiczba)
	{
		gracz->dodajHajs(9*stawka);
		dialog->PoinformujoWygranej(10*stawka);
	}
	else
	{
		gracz->odejmijHajs(stawka);
		dialog->PoinformujoPrzegranej(stawka);
	}

}

int Ruletka::WylosujLiczbe()
{
	int wylosowanaLiczba = Narzedzia::LosujLiczbe(0, 36);
	cout << wylosowanaLiczba;
	return wylosowanaLiczba;
}

	

