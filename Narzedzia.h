#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <ctime>

using namespace std;

class Narzedzia
{
public:
	static int LosujLiczbe(int min, int max);
	static string IntToString(int liczba);

private:
	Narzedzia();
	
};

