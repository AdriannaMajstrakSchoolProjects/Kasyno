#include "stdafx.h"
#include "Top100.h"
#include "Zawodnik.h"
#include "narzedzia.h"
#include <stdlib.h> 
#include <vector> 

using namespace std;


Top100::Top100(ObslugaPliku * objObslugaPliku)
{
	obslugaPliku = objObslugaPliku;
}

void Top100::Sortowanie(vector<Zawodnik*> &zawodnicy)
{
	bool flaga = false;
	Zawodnik* przechowalnia;

	do
	{
		flaga = false;

		for (size_t i = 0; i < zawodnicy.size() - 1; i++)
		{
			if (zawodnicy[i]->wygrana < zawodnicy[i + 1]->wygrana)
			{
				flaga = true;
				przechowalnia = zawodnicy[i];
				zawodnicy[i] = zawodnicy[i + 1];
				zawodnicy[i + 1] = przechowalnia;
			}
		}
	} while (flaga);
}

Zawodnik * stworzZawodnika(string &zmImie, string &zmWynik)
{
	Zawodnik* nowy = new Zawodnik();
	nowy->login = zmImie;
	nowy->wygrana = stoi(zmWynik);
	zmImie = "";
	zmWynik = "";
	return nowy;
	
	 
}

void Top100::ZapiszWynik(string imie, int wynik)
{
	string plikZWynikami = obslugaPliku->wczytajPlik();
	string zmImie = "";
	string zmWynik = "";
	bool wczytywanieImienia = true;
	vector<Zawodnik*> zawodnicy;
	Zawodnik * biezacy = new Zawodnik();
	biezacy->login = imie;
	biezacy->wygrana = wynik;
	zawodnicy.push_back(biezacy);

	for (size_t i = 0; i < plikZWynikami.length(); i++)
	{		
		if (plikZWynikami[i] == ' ')
		{
			wczytywanieImienia = false;
		}
		else if(plikZWynikami[i] == '\r')
		{
			wczytywanieImienia = true;
		}
		else if(plikZWynikami[i] == '\n')
		{
			zawodnicy.push_back(stworzZawodnika(zmImie, zmWynik));
		}
		else if(wczytywanieImienia)
		{
			zmImie += plikZWynikami[i];
		}
		else if (!wczytywanieImienia)
		{
			zmWynik += plikZWynikami[i];
		}
	}

	if (zmImie != "")
	{
		zawodnicy.push_back(stworzZawodnika(zmImie, zmWynik));
	}

	Sortowanie(zawodnicy);

	string gotowaLista = "";

	////Algorytm pozwalajacy na zapisanie 100 pierwszych zawodnikow
	for (size_t i = 0; i < zawodnicy.size(); i++)
	{
		if (i > 100)
		{
			break;
		}

		gotowaLista += zawodnicy[i]->login;
		gotowaLista += " ";
		gotowaLista += Narzedzia::IntToString(zawodnicy[i]->wygrana);
		gotowaLista += "\r\n";
		////Moze byc enter na koncu pliku poniewaz jest zabezpieczenie przy wczytywaniu
	}

	obslugaPliku->zapiszDoPliku(gotowaLista);


	//// TODO zapisa� pierwszych 100 w pliku


}




