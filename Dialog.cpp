#include "stdafx.h"
#include "Dialog.h"
#include "Zawodnik.h"

#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <conio.h>
#include <chrono>
#include <thread>

using namespace std;

////Metoda dzia�a dla cyfr 0-9.
int Dialog::PobieranieCyfryOdUzytkownika(int min, int max) 
{
	int kodASCII;

	do
	{
		kodASCII = _getch();

	} while (kodASCII < (48 + min) || kodASCII > (48 + max));

	////Bo Ascii dla znaku 0 r�wne jest 48, a dla 1 = 49 itd.
	return kodASCII - 48;
}

int Dialog::PobieranieLiczbyOdUzytkownika(int min, int max)
{
	int liczba;
	do
	{
		cin >> liczba;
		if (cin.fail())
		{
			cout << "To nie liczba!" << endl;
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
		else if (liczba<min || liczba>max)
		{
			cout << "Liczba z poza zakresu" << endl;
		}
		

	} while (liczba<min || liczba>max);

	return liczba;
}
void Dialog::Oczekiwanie()
{
	for (size_t i = 0; i < 10; i++)
	{
		this_thread::sleep_for(chrono::milliseconds(200));
		system("cls");

		for (size_t j = 0; j < i; j++)
		{
			cout << " ";

		}

		cout << "o";
	};

	cout << endl;
}
int Dialog::wyborGryZMenu()
{
	system("cls");
	cout << "Aby wybrac gre wcisnij odpowiednia cyfre" << endl;
	cout << "1 - Black Jack" << endl;
	cout << "2- jednoreki bandyta" << endl;
	cout << "3 - ruletka" << endl;
	cout << "4 - wyniki Top 100 graczy" << endl;
	cout << "5 - zakoncz" << endl;

	return PobieranieCyfryOdUzytkownika(1, 5);
}

Zawodnik* Dialog::PrezentacjaDanychZawodnika(Zawodnik* zawodnik)
{
	cout << "Witaj " << zawodnik->login << endl;
	PrezentajaPortfelaZawodnika(zawodnik);
	cout << "Milej zabawy :)" << endl;
	system("pause");

	return zawodnik;
}

void Dialog::PrezentajaPortfelaZawodnika(Zawodnik * zawodnik)
{
	cout << "Masz " << zawodnik->hajs << " gotowki." << endl;
}

int Dialog::PobierzStawke(int masymalnaStawka)
{
	int stawka;

	do 
	{
		cout << "Za ile chcesz zagra�?" << endl;

		cin >> stawka;	
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		if (stawka > masymalnaStawka) {
			cout << "Nie masz tyle!" << endl;
		}


		if (stawka <= 0) {
			cout << "Podaj kwote wieksza od 0 i mniejsza niz 32 bity :D" << endl;
		}
		
	} while (stawka > masymalnaStawka || stawka <= 0);
	
	return stawka;
}

void Dialog::PoinformujoWygranej(int kwota)
{
	cout << "Wygra�e�: " << kwota << endl;
}

void Dialog::PoinformujoPrzegranej(int kwota)
{
	cout << "Przegrales :( Staciles: " << kwota << endl;
}

bool Dialog::CzyGraszDalej()
{
	cout << "Czy chcesz gra� dalej w t� gr�?" << endl;
	cout << "1 - tak" << endl;
	cout << "2 - nie" << endl;

	return PobieranieCyfryOdUzytkownika(1, 2) == 1;	
}






