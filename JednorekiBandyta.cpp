#include "stdafx.h"
#include "JednorekiBandyta.h"
#include "Narzedzia.h"
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

JednorekiBandyta::JednorekiBandyta(Dialog * dialogPtr):Gra(dialogPtr)
{
}

void JednorekiBandyta::Graj()
{
	cout << "Grsz w Jednorekiego. Podaj stawke.";
	cout << "Jesli trafisz 3 takie same symbole podwajasz stawke, je�li nie trafisz tracisz kase.";
	cout << endl;

	dialog->PrezentajaPortfelaZawodnika(gracz);
	int stawka = dialog->PobierzStawke(gracz->hajs);

	
	int okno1= Narzedzia::LosujLiczbe(1,9);
	int okno2 = Narzedzia::LosujLiczbe(1, 9);
	int okno3 = Narzedzia::LosujLiczbe(1, 9);

	cout << "| " << okno1 << " | " << okno2 << " | " << okno3 << " |" << endl;

	if (okno1 == okno2 && okno2 == okno3)
	{
		dialog->PoinformujoWygranej(stawka);
		gracz->dodajHajs(stawka);
	}
	else
	{
		dialog->PoinformujoPrzegranej(stawka);
		gracz->odejmijHajs(stawka);
	}


	if (czyWyplacalny() && dialog->CzyGraszDalej())
	{
		Graj();
	}
}