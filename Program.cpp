//-----------------------------------------------------------------------
// <copyright file="NameOfFile.cs" company="CompanyName">
//     Company copyright tag.
// </copyright>
//-----------------------------------------------------------------------

#include "stdafx.h"
#include "Gra.h"
#include "BlackJack.h"
#include "ruletka.h"
#include "jednorekiBandyta.h"
#include "Dialog.h"
#include "Zawodnik.h"
#include "Narzedzia.h"
#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "Program.h"
#include "Top100.h"
#include "ObslugaPliku.h"

#include <direct.h>

using namespace std;

int main()
{	
	srand((unsigned int)time(NULL));
	
	Dialog *dialog = new Dialog;
	ObslugaPliku * obsluga = new ObslugaPliku("Top100.txt");


	Zawodnik *zawodnik = new Zawodnik;
	cout << "Podaj nick" << endl;
	
	cin>>zawodnik->login;	
	zawodnik->startHajs = Narzedzia::LosujLiczbe(1,1000);	
	zawodnik->hajs = zawodnik->startHajs;
	dialog->PrezentacjaDanychZawodnika(zawodnik);

	
	vector<Gra*> listaGier;
	listaGier.push_back(new BlackJack(dialog));
	listaGier.push_back(new JednorekiBandyta(dialog));
	listaGier.push_back(new Ruletka(dialog));
	
	int numerGry;
	
	do
	{
		numerGry = dialog->wyborGryZMenu();

		if (numerGry == 5)
		{
			break;
		}
		else if (numerGry == 4)
		{
			system("cls");
			cout<<obsluga->wczytajPlik();
			system("pause");

		}
		else
		{
			//// Gry numerujemy od 1, a lista jest numerowana od 0.
			listaGier[numerGry - 1]->Podejdz(zawodnik);
			listaGier[numerGry - 1]->Graj();
			listaGier[numerGry - 1]->Odejdz();
		}

		
	} while (zawodnik->hajs > 0);

	//// Sprzątanie obiektow
	for (size_t i = 0; i < listaGier.size(); i++)
	{
		delete listaGier[i];
	}

	Top100 * lista = new Top100(obsluga);

	lista->ZapiszWynik(zawodnik->login, zawodnik->wygrana);

	delete dialog;
	delete zawodnik;
	delete lista;
	delete obsluga;
	
	system("pause");
	return 0;
}