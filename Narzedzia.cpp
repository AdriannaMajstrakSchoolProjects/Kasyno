#include "stdafx.h"
#include "Narzedzia.h"
#include <string>   
#include <iostream>    
#include <sstream> 
using namespace std;




int Narzedzia::LosujLiczbe(int min, int max)
{

	int wylosowanaLiczba = min + (rand() % (int)(max - min + 1));
	return wylosowanaLiczba;
}

string Narzedzia::IntToString(int liczba)
{
	stringstream convert; 

	convert << liczba;
	
	return convert.str(); ;
}
