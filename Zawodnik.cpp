#include "stdafx.h"
#include "Zawodnik.h"


Zawodnik::Zawodnik()
{
	startHajs = 0;
	hajs = 0;
	wygrana = 0;
}

void Zawodnik::dodajHajs(int ile)
{
	hajs += ile;
}

void Zawodnik::odejmijHajs(int ile)
{
	hajs -= ile;
}

void Zawodnik::obliczWygrana()
{
	wygrana = hajs-startHajs;
}
